<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="scene_objects.simulation_cell"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>Simulation cell</title>

  <para>
    <informalfigure><screenshot><mediaobject><imageobject>
       <imagedata fileref="images/scene_objects/simulation_cell_panel.png" format="PNG" scale="50" />
    </imageobject></mediaobject></screenshot></informalfigure>

    This <link linkend="scene_objects">object</link> represents the geometry of the two- or three-dimensional simulation domain
    and the boundary conditions (PBC flags). You can find the cell object that was loaded from the imported simulation file in the 
    <emphasis>Data source</emphasis> section of the <link linkend="usage.modification_pipeline.pipeline_listbox">pipeline editor</link> (see screenshot).
    Note that the pipeline editor usually shows another item also called <emphasis>Simulation cell</emphasis>.
    This is the corresponding <link linkend="visual_elements.simulation_cell">visual element</link> controlling the appearance of the simulation cell in rendered images.
  </para>

  <para>
    Some simulation file formats do not store the dimensions of the simulation cell that was used in the original simulation.
    If OVITO cannot find the cell dimensions in the imported file, it automatically generated an ad-hoc simulation box that corresponds to the axis-aligned 
    bounding box of all particle coordinates.
  </para>

  <simplesect>
    <title>Dimensionality</title>
    <para>
      OVITO supports 2- and 3-dimensional datasets. The <emphasis>dimensionality</emphasis> detected by OVITO during file import can be 
      overridden by the user in the simulation cell panel.
      In "2D" mode, the z-coordinates of particles and the third simulation cell vector will be ignored by most computations performed by OVITO.
    </para>
  </simplesect>

  <simplesect>
    <title>Boundary conditions</title>
    <para>
      The <emphasis>periodic boundary condition</emphasis> (PBC) flags of the simulation cell determine whether OVITO performs calculations within
      a periodic domain or not. If possible, OVITO tries to read or guess these flags from the imported simulation file, 
      but you can manually override them here if needed.
    </para>
  </simplesect>

  <simplesect>
    <title>Geometry</title>
    <para>
      The shape of the three-dimensional simulation cell is defined by three vectors spanning a parallelepiped and the coordinates of 
      one corner of the parallelepiped (the cell's origin).
      The panel displays the original shape information loaded from the imported simulation file. 
      To modify the simulation cell vectors, you can insert the <link linkend="particles.modifiers.affine_transformation">Affine transformation</link> modifier
      into the data pipeline.
    </para>
  </simplesect>

  <simplesect>
  <title>See also</title>
  <para>
      <pydoc-link href="modules/ovito_data" anchor="ovito.data.SimulationCell"><classname>SimulationCell</classname> (Python API)</pydoc-link>
  </para>
  </simplesect>

</section>