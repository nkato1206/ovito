.. _credits:

============================
Software license information
============================

*OVITO Basic* in binary form is published by the software vendor `OVITO GmbH, Germany <https://www.ovito.org>`__ under the terms of the `MIT License <https://gitlab.com/stuko/ovito/-/blob/master/LICENSE.MIT.txt>`__. 
Program packages are built from source code available under the terms of the :ref:`GNU General Public License (v3) <appendix.GPL>` and the 
`MIT License <https://gitlab.com/stuko/ovito/-/blob/master/LICENSE.MIT.txt>`__. The source code is hosted in a public `GitLab repository <https://gitlab.com/stuko/ovito/>`__.

*OVITO Pro* is proprietary software of OVITO GmbH and licensed under the terms of the *OVITO Pro End-User Licence Agreement*.
See :ref:`this page <credits.ovito_pro>` for more information on the differences between the two versions of OVITO.

The ``ovito`` Python module, distributed as PyPI and Anaconda packages, is made available by OVITO GmbH under the `MIT License <https://gitlab.com/stuko/ovito/-/blob/master/LICENSE.MIT.txt>`__.

Third-party software
====================

*OVITO Pro* and *OVITO Basic* are based on the following third-party software components. Click the links to see the corresponding copyright notices and/or build instructions.

.. list-table::
  :width: 100%
  :widths: auto
  :header-rows: 1

  * - Library
    - License
    - Instructions

  * - OVITO legacy code
    - :ref:`license <appendix.license.ovito_legacy>`
    - 

  * - `Qt <https://www.qt.io/developers/>`__
    - LGPL v3
    - :ref:`Info <appendix.license.qt.instructions>`

  * - `Qwt <https://sourceforge.net/projects/qwt/>`__
    - :ref:`license <appendix.license.qwt>`
    - 

  * - `Boost <https://www.boost.org>`__
    - :ref:`license <appendix.license.boost>`
    - 

  * - `Python <https://www.python.org>`__
    - PSF license
    - 

  * - `Geogram library <http://alice.loria.fr/index.php/software/4-library/75-geogram.html>`__ by Bruno Levy, INRIA - ALICE
    - :ref:`license <appendix.license.geogram>`
    - 

  * - `Tachyon Parallel / Multiprocessor Ray Tracing System <https://en.wikipedia.org/wiki/Tachyon_(software)>`__ by John E. Stone
    - :ref:`license <appendix.license.tachyon>`
    - 

  * - `muParser <http://beltoforion.de/article.php?a=muparser>`__ by Ingo Berg
    - :ref:`license <appendix.license.muparser>`
    - 

  * - `Libssh <https://libssh.org>`__
    - LGPL v2.1
    - :ref:`Info <appendix.license.libssh.instructions>`

  * - `OpenSSL <https://openssl.org>`__
    - :ref:`license <appendix.license.openssl>`
    - 

  * - `ffmpeg <https://ffmpeg.org>`__
    - LGPL v2.1
    - :ref:`Info <appendix.license.ffmpeg.instructions>`

  * - `netCDF <https://www.unidata.ucar.edu/software/netcdf/>`__
    - :ref:`license <appendix.license.netcdf>`
    - 

  * - `HDF5 <https://support.hdfgroup.org/HDF5/>`__
    - :ref:`license <appendix.license.hdf5>`
    - 

  * - `Voro++ <https://doi.org/10.1063/1.3215722>`__ by Chris Rycroft
    - :ref:`license <appendix.license.voroplusplus>`
    - 

  * - `zlib <https://www.zlib.net>`__
    - `license <https://www.zlib.net/zlib_license.html>`__
    - 

  * - `Polyhedral Template Matching library <https://github.com/pmla/polyhedral-template-matching>`__ by Peter Mahler Larsen
    - :ref:`license <appendix.license.ptm>`
    - 

  * - `QCP rotation calculation method <https://dx.doi.org/10.1002%2Fjcc.21439>`__ by Pu Liu and Douglas L. Theobald
    - :ref:`license <appendix.license.qcprot>`
    - 

  * - `GSD (General Simulation Data) <https://gsd.readthedocs.io/>`__ I/O routines
    - :ref:`license <appendix.license.gsd>`
    - 

  * - `pybind11 <https://pybind11.readthedocs.io/>`__
    - :ref:`license <appendix.license.pybind11>`
    - 

  * - `PySide2 <https://pypi.org/project/PySide2/>`__
    - LGPL v3
    - :ref:`Info <appendix.license.pyside2.instructions>`

  * - `Numpy <https://www.numpy.org/>`__
    - :ref:`license <appendix.license.numpy>`
    - 

  * - `Matplotlib <https://matplotlib.org>`__
    - `license <https://matplotlib.org/users/license.html>`__
    - 

  * - `KISS FFT <https://github.com/mborgerding/kissfft>`__
    - :ref:`license <appendix.license.kissfft>`
    - 

  * - `OSPRay <https://www.ospray.org>`__
    - Apache 2.0
    - 

  * - `Embree <https://embree.github.io>`__
    - Apache 2.0
    - 

  * - `Intel Open Image Denoise <https://openimagedenoise.github.io/>`__
    - Apache 2.0
    - 

  * - `Intel Open Volume Kernel Library <https://www.openvkl.org>`__
    - Apache 2.0
    - 

  * - `Intel RenderKit common C++/CMake infrastructure <https://github.com/ospray/rkcommon>`__
    - Apache 2.0
    - 

  * - `Threading Building Blocks <https://www.threadingbuildingblocks.org/>`__
    - Apache 2.0
    - 

  * - `GEMMI (GEneral MacroMolecular I/O) <https://github.com/project-gemmi/gemmi>`__
    - MPL v2
    - 

  * - `Parsing Expression Grammar Template Library (PEGTL) <https://github.com/taocpp/PEGTL/>`__
    - :ref:`license <appendix.license.pegtl>`
    - 

  * - XTC/XDR file I/O routines
    - :ref:`license <appendix.license.xdrfile>`
    - 

  * - `Vulkan Memory Allocator <https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator>`__
    - :ref:`license <appendix.license.vma>`
    - 

  * - `LAMMPS <https://www.lammps.org//>`__
    - LGPL v3
    - 

.. toctree::
  :hidden:

  ovito_legacy
  GPL
  FDL
  Qt.instructions
  tachyon
  muparser
  netcdf
  hdf5
  numpy
  geogram
  GSD
  PTM
  pybind11
  kissfft
  qcprot
  voroplusplus
  boost
  qwt
  openssl
  libssh.instructions
  ffmpeg.instructions
  pegtl
  PySide2.instructions
  xdrfile
  vma
