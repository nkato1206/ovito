.. _viewport_layers.coordinate_tripod:

Coordinate tripod viewport layer
--------------------------------

.. image:: /images/viewport_layers/coordinate_tripod_overlay_panel.*
  :width: 30%
  :align: right

This :ref:`viewport layer <viewport_layers>` renders a tripod into the picture to 
indicate the orientation of the simulation coordinate system.

.. image:: /images/viewport_layers/coordinate_tripod_example.*
  :width: 30%

.. seealso::

  :py:class:`ovito.vis.CoordinateTripodOverlay` (Python API)