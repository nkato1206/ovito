#######################################################################################
#
#  Copyright 2021 OVITO GmbH, Germany
#
#  This file is part of OVITO (Open Visualization Tool).
#
#  OVITO is free software; you can redistribute it and/or modify it either under the
#  terms of the GNU General Public License version 3 as published by the Free Software
#  Foundation (the "GPL") or, at your option, under the terms of the MIT License.
#  If you do not alter this notice, a recipient may use your version of this
#  file under either the GPL or the MIT License.
#
#  You should have received a copy of the GPL along with this program in a
#  file LICENSE.GPL.txt.  You should have received a copy of the MIT License along
#  with this program in a file LICENSE.MIT.txt
#
#  This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
#  either express or implied. See the GPL or the MIT License for the specific language
#  governing rights and limitations.
#
#######################################################################################

# Define plugin module.
OVITO_STANDARD_PLUGIN(Gui
	SOURCES
		mainwin/MainWindow.cpp
		mainwin/ViewportsPanel.cpp
		mainwin/TaskDisplayWidget.cpp
		mainwin/cmdpanel/CommandPanel.cpp
		mainwin/cmdpanel/RenderCommandPage.cpp
		mainwin/cmdpanel/ModifyCommandPage.cpp
		mainwin/cmdpanel/OverlayCommandPage.cpp
		mainwin/data_inspector/DataInspectorPanel.cpp
		mainwin/data_inspector/DataInspectionApplet.cpp
		mainwin/data_inspector/GlobalAttributesInspectionApplet.cpp
		dialogs/AdjustViewDialog.cpp
		dialogs/AnimationKeyEditorDialog.cpp
		dialogs/AnimationSettingsDialog.cpp
		dialogs/ApplicationSettingsDialog.cpp
		dialogs/GeneralSettingsPage.cpp
		dialogs/ViewportSettingsPage.cpp
		dialogs/ModifierTemplatesPage.cpp
		dialogs/HistoryFileDialog.cpp
		dialogs/ImportFileDialog.cpp
		dialogs/ImportRemoteFileDialog.cpp
		dialogs/SaveImageFileDialog.cpp
		dialogs/LoadImageFileDialog.cpp
		dialogs/RemoteAuthenticationDialog.cpp
		dialogs/FileExporterSettingsDialog.cpp
		dialogs/ClonePipelineDialog.cpp
		dialogs/FontSelectionDialog.cpp
		dialogs/ModalPropertiesEditorDialog.cpp
		actions/WidgetActionManager.cpp
		actions/FileActions.cpp
		actions/RenderActions.cpp
		actions/SearchActions.cpp
		dataset/GuiDataSetContainer.cpp
		dataset/io/FileSourceEditor.cpp
		dataset/io/FileSourcePlaybackRateEditor.cpp
		dataset/io/FileImporterEditor.cpp
		dataset/io/AttributeFileExporterEditor.cpp
		dataset/animation/controller/TCBInterpolationControllerEditor.cpp
		widgets/general/RolloutContainer.cpp
		widgets/general/RolloutContainerLayout.cpp
		widgets/general/SpinnerWidget.cpp
		widgets/general/ColorPickerWidget.cpp
		widgets/general/ElidedTextLabel.cpp
		widgets/general/AutocompleteLineEdit.cpp
		widgets/general/AutocompleteTextEdit.cpp
		widgets/general/HtmlListWidget.cpp
		widgets/general/ViewportModeButton.cpp
		widgets/general/StatusBar.cpp
		widgets/general/PopupUpdateComboBox.cpp
		widgets/animation/AnimationTimeSpinner.cpp
		widgets/animation/AnimationFramesToolButton.cpp
		widgets/animation/AnimationTimeSlider.cpp
		widgets/animation/AnimationTrackBar.cpp
		widgets/rendering/FrameBufferWidget.cpp
		widgets/rendering/FrameBufferWindow.cpp
		widgets/selection/SceneNodesListModel.cpp
		widgets/selection/SceneNodeSelectionBox.cpp
		widgets/display/CoordinateDisplayWidget.cpp
		widgets/display/StatusWidget.cpp
		properties/PropertiesEditor.cpp
		properties/PropertiesPanel.cpp
		properties/ParameterUI.cpp
		properties/NumericalParameterUI.cpp
		properties/AffineTransformationParameterUI.cpp
		properties/BooleanActionParameterUI.cpp
		properties/BooleanGroupBoxParameterUI.cpp
		properties/BooleanParameterUI.cpp
		properties/BooleanRadioButtonParameterUI.cpp
		properties/ColorParameterUI.cpp
		properties/CustomParameterUI.cpp
		properties/DefaultPropertiesEditor.cpp
		properties/FilenameParameterUI.cpp
		properties/FloatParameterUI.cpp
		properties/FontParameterUI.cpp
		properties/IntegerParameterUI.cpp
		properties/IntegerRadioButtonParameterUI.cpp
		properties/IntegerCheckBoxParameterUI.cpp
		properties/ModifierPropertiesEditor.cpp
		properties/ModifierDelegateParameterUI.cpp
		properties/ModifierDelegateFixedListParameterUI.cpp
		properties/ModifierDelegateVariableListParameterUI.cpp
		properties/RefTargetListParameterUI.cpp
		properties/StringParameterUI.cpp
		properties/SubObjectParameterUI.cpp
		properties/VariantComboBoxParameterUI.cpp
		properties/Vector3ParameterUI.cpp
		properties/OpenDataInspectorButton.cpp
		properties/ModifierGroupEditor.cpp
		rendering/RenderSettingsEditor.cpp
		rendering/StandardSceneRendererEditor.cpp
		viewport/ViewportMenu.cpp
		viewport/WidgetViewportWindow.cpp
		viewport/input/XFormModes.cpp
		viewport/overlays/CoordinateTripodOverlayEditor.cpp
		viewport/overlays/TextLabelOverlayEditor.cpp
		viewport/overlays/MoveOverlayInputMode.cpp
		app/GuiApplication.cpp
		app/GuiApplicationService.cpp
		utilities/io/GuiFileManager.cpp
		utilities/concurrent/ProgressDialog.cpp
		resources/gui.qrc
	PLUGIN_DEPENDENCIES
		GuiBase
	LIB_DEPENDENCIES
		${OVITO_QT_MAJOR_VERSION}::Widgets 
		${OVITO_QT_MAJOR_VERSION}::Network
	PRECOMPILED_HEADERS
		GUI.h
)

IF(OVITO_QT_MAJOR_VERSION STREQUAL "Qt6")
	# QOpenGLWidget class has been moved to the OpenGLWidgets module in Qt 6.
	TARGET_LINK_LIBRARIES(Gui PUBLIC ${OVITO_QT_MAJOR_VERSION}::OpenGLWidgets)
ENDIF()

# Inject copyright notice text into the source code.
SET_PROPERTY(SOURCE actions/FileActions.cpp
	APPEND PROPERTY COMPILE_DEFINITIONS
	"OVITO_COPYRIGHT_NOTICE=\"${OVITO_COPYRIGHT_NOTICE}\"")

# Let the GUI module know if we are building a preview/development version of OVITO.
# A notice will be shown in the main window reminding the user to switch to the
# final release once it becomes available.
IF(OVITO_USE_GIT_REVISION_NUMBER)
	STRING(TIMESTAMP current_date "%d-%b-%Y")
	TARGET_COMPILE_DEFINITIONS(Gui PRIVATE "OVITO_DEVELOPMENT_BUILD_DATE=\"${current_date}\"")
ENDIF()

# Compute the relative path where the documentation files get installed relative to the executable. 
FILE(RELATIVE_PATH OVITO_DOCUMENTATION_PATH "${OVITO_BINARY_DIRECTORY}" "${OVITO_SHARE_DIRECTORY}/doc/manual/html/")
TARGET_COMPILE_DEFINITIONS(Gui PRIVATE "OVITO_DOCUMENTATION_PATH=\"${OVITO_DOCUMENTATION_PATH}\"")

# Propagate list of plugins to parent scope.
SET(OVITO_PLUGIN_LIST ${OVITO_PLUGIN_LIST} PARENT_SCOPE)
